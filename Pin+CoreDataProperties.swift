//
//  Pin+CoreDataProperties.swift
//  SimplePin
//
//  Created by Dmytro Lohush on 10/6/16.
//  Copyright © 2016 Dmytro Lohush. All rights reserved.
//

import Foundation
import CoreData


extension Pin {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pin> {
        return NSFetchRequest<Pin>(entityName: "Pin");
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var id: Int64
    @NSManaged public var address: String?

}

//
//  User+CoreDataProperties.swift
//  SimplePin
//
//  Created by Dmytro Lohush on 10/6/16.
//  Copyright © 2016 Dmytro Lohush. All rights reserved.
//

import Foundation
import CoreData

extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var facebookId: Int64
    @NSManaged public var accessToken: String?
    @NSManaged public var pins: NSSet?

}

//
//  AppDelegate.swift
//  SimplePin
//
//  Created by Dmytro Lohush on 10/4/16.
//  Copyright © 2016 Dmytro Lohush. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
//                newUser.setValue(1, forKey: "facebookId")
//                newUser.setValue("super", forKey: "accessToken")
//                do {
//                    try newUser.managedObjectContext?.save()
//                } catch {
//                    print(error)
//                }
        // Override point for customization after application launch.
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<User>()
        
        // Create Entity Description
//        let entityDescription = NSEntityDescription.entity(forEntityName: "User", in: self.persistentContainer.viewContext)
//        let pinDescription = NSEntityDescription.entity(forEntityName: "Pin", in: self.persistentContainer.viewContext)
//        let newPin = NSManagedObject(entity: pinDescription!, insertInto: self.persistentContainer.viewContext)
//        newPin.setValue(2.0, forKey: "latitude")
//        newPin.setValue(2, forKey: "id")
//        newPin.setValue(-20.24, forKey: "longitude")
//        newPin.setValue("lviv", forKey: "address")
//        do {
//            try newPin.managedObjectContext?.save()
//        } catch {
//            print(error)
//        }
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            let result = try self.persistentContainer.viewContext.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            if (result.count > 0) {
                let person = result[0] as! User
                
                print("1 - \(person)")
                
                if let facebookId = person.value(forKey: "facebookId"), let accessToken = person.value(forKey: "accessToken") {
                    print("\(facebookId) \(accessToken)")
                }
            
//                person.pins = NSSet(object: newPin)
                if let pins = person.pins{
                    print("success")
                    let pin = (pins.allObjects as! [Pin]).first
                    print(pin?.address)
                    
                }
                print("2 - \(person)")
                
            }

            print(result)
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
//        do {
//            try newUser.managedObjectContext?.save()
//        } catch {
//            print(error)
//        }
//        

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "SimplePin")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


//
//  ViewController.swift
//  SimplePin
//
//  Created by Dmytro Lohush on 10/4/16.
//  Copyright © 2016 Dmytro Lohush. All rights reserved.
//

import UIKit
import MapKit
import Accounts
import Social
import CoreData

class ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var mapView: MKMapView!
    
    var facebookAccount: ACAccount?
    let manager = CLLocationManager()
    var isLoggedIn = false
    var currentUser: NSManagedObject?{
        didSet{
            setupMapViewWith(pins: (currentUser as! User).pins)
        }
    }
    
    var appDelegate: AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var managedContext: NSManagedObjectContext{
        return appDelegate.persistentContainer.viewContext
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        
        setupLocationManager()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    @IBAction func handleLongPress(_ sender: AnyObject) {
        if sender.state == UIGestureRecognizerState.began {
            let location = sender.location(in: mapView)
            let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = "test"
            mapView.addAnnotation(annotation)
        }
    }
    
    // MARK: - Custom Methods
    private func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.applicationDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    private func setupMapViewWith(pins: NSSet?) {
        mapView.delegate = self
        mapView.showsUserLocation = true
        if pins != nil{
            for pin in pins! {                
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: (pin as! Pin).latitude, longitude: (pin as! Pin).longitude)
                annotation.title = pin as! Pin
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    private func setupLocationManager() {
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    private func facebookLogin(completion: @escaping () -> Void) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let store = ACAccountStore()
            let facebook = store.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierFacebook)
            let appId = "1842838905951945"
            let readPermissions = ["public_profile", "email"]
            let loginParameters: [NSObject: AnyObject] = [ACFacebookAppIdKey as NSObject: appId as AnyObject, ACFacebookPermissionsKey as NSObject: readPermissions as AnyObject]
            store.requestAccessToAccounts(with: facebook, options: loginParameters) { granted, error in
                if granted {
                    self.isLoggedIn = true
                    let accounts = store.accounts(with: facebook)
                    self.facebookAccount = accounts?.last as? ACAccount
                    let credentials = self.facebookAccount?.credential
                    completion()
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.askForSettingsChange(title: "Please grant access to Facebook", message: "Permission not granted For Your Application")
                    })
                }
            }
        }else{
            self.askForSettingsChange(title: "Please login to a Facebook", message: "Go to settings to login Facebook")
        }
    }
    
    private func askForSettingsChange(title : String? , message : String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingAction = UIAlertAction(title: "Settings", style: .default) { (action : UIAlertAction) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                UIApplication.shared.openURL((NSURL(string: UIApplicationOpenSettingsURLString)! as URL)) // go to settings
            })
        }
        
        
        alertController.addAction(settingAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func checkLogin() {
        facebookLogin {
            let id = Int64(self.facebookAccount?.value(forKeyPath: "properties.uid") as! Int64)
            if let user = self.getUserFor(id: id){
                self.currentUser = user
            }else{
                self.createUserWith(id: id)
            }
        }
    }
    
    private func createUserWith(id: Int64) {
        let userEntityDescription = NSEntityDescription.entity(forEntityName: "User", in: self.managedContext)
        let newUser = NSManagedObject(entity: userEntityDescription!, insertInto: appDelegate.persistentContainer.viewContext)
        newUser.setValue(id, forKey: "facebookId")
        self.currentUser = newUser
        do {
            try newUser.managedObjectContext?.save()
        } catch {
            print(error)
        }
    }
    
    private func getUserFor(id: Int64) -> User?{
        let userEntityDescription = NSEntityDescription.entity(forEntityName: "User", in: self.managedContext)
        let fetchRequest = NSFetchRequest<User>()
        fetchRequest.entity = userEntityDescription
        do {
            let results =
                try self.managedContext.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            var users = results as! [User]
            users = users.filter{$0.facebookId == id}
            if users.count > 0 {
                return users.first!
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    func applicationDidBecomeActive(notification: NSNotification) {
        checkLogin()
    }
    
    
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        var region = MKCoordinateRegion()
        region.center = (locations.last?.coordinate)!
        var span = MKCoordinateSpan()
        span.latitudeDelta  = 0.01
        span.longitudeDelta = 0.01
        region.span = span
        self.mapView.setRegion(region, animated: true)
        self.mapView.regionThatFits(region)
        manager.stopUpdatingLocation()
    }
    
}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        mapView.removeAnnotation(view.annotation!)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: MKUserLocation.self){
            return nil;
        }
        var view = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
        if (view != nil) {
            view?.annotation = annotation;
        } else {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            view?.canShowCallout = true
            let deleteButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            deleteButton.setImage(UIImage(named: "trashCan"), for: .normal)
            view?.rightCalloutAccessoryView = deleteButton
        }
        
        return view
    }
    
}

